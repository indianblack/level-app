package com.example.levelapp;

import androidx.appcompat.app.AppCompatActivity;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);

        Sensor acc = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        sm.registerListener(new SensorEventListener() {
            float previous[] = {0.0f, 0.0f, 0.0f};
            float alpha = 0.1f;
            boolean isFirstTime = true;

            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {

                if (isFirstTime) {
                    previous[0] = sensorEvent.values[0];
                    previous[1] = sensorEvent.values[1];
                    previous[2] = sensorEvent.values[2];
                    isFirstTime = false;
                } else {
                    previous[0] = alpha * sensorEvent.values[0] + (1.0f - alpha) * previous[0];
                    previous[1] = alpha * sensorEvent.values[1] + (1.0f - alpha) * previous[1];
                    previous[2] = alpha * sensorEvent.values[2] + (1.0f - alpha) * previous[2];
                }

                float accel[] = new float[3];
                accel[0] = sensorEvent.values[0] - previous[0];
                accel[1] = sensorEvent.values[1] - previous[1];
                accel[2] = sensorEvent.values[2] - previous[2];

                MyBestSurfaceView sv = findViewById(R.id.mySurface);
                sv.setPosXFromGravity(previous[0]);
                sv.setPosYFromGravity(previous[1]);
                sv.invalidate();
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        }, acc, SensorManager.SENSOR_DELAY_UI);
    }
}