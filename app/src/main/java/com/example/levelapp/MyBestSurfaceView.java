package com.example.levelapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceView;

public class MyBestSurfaceView extends SurfaceView {
    public float pos_x = 0.0f;
    public float pos_y = 0.0f;

    private void init() {
        setWillNotDraw(false);
    }

    public void setPosXFromGravity(float gravity_x) {
        if (gravity_x > 9.81f) {
            gravity_x = 9.81f;
        } else if (gravity_x < -9.81f) {
            gravity_x = -9.81f;
        }
        pos_x = (getWidth() / 2.0f) * (gravity_x / 9.81f);
    }

    public void setPosYFromGravity(float gravity_y) {
        if (gravity_y > 9.81f) {
            gravity_y = 9.81f;
        } else if (gravity_y < -9.81f) {
            gravity_y = -9.81f;
        }
        pos_y = (getHeight() / 2.0f) * (gravity_y / 9.81f);
    }

    public MyBestSurfaceView(Context context) {
        super(context);
        init();
    }

    public MyBestSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyBestSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public MyBestSurfaceView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public static Float setRadiusSize(float pos_x, float pos_y, Canvas canvas) {
        float resize = (float) ((Math.abs(pos_y) + Math.abs(pos_x) + 3531.6d) / 3531.6d);
        float radius = (Math.min(canvas.getHeight(), canvas.getWidth()) / 15.0f) / resize;

        return radius;
    }

    public static Integer setOpacity(float pos_x, float pos_y, float center_x, float center_y) {
        double distance = Math.sqrt((0 - pos_y) * (0 - pos_y) + (0 - pos_x) * (0 - pos_x));
        float denominator = (center_x + center_y) / 255;
        float hue = (float) (distance + center_x + center_y) / denominator;
        int alpha = (int)(255 * (255 / hue));

        return alpha;

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint paint = new Paint();

        canvas.drawRGB(255, 255, 255);
        float center_x = canvas.getWidth() / 2.0f;
        float center_y = canvas.getHeight() / 2.0f;


        float radius_big = Math.min(canvas.getHeight(), canvas.getWidth()) / 15.0f;

        paint.setColor(Color.RED);
        paint.setStrokeWidth(6.0f);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(center_x, center_y, radius_big, paint);

        paint.setAlpha(setOpacity(pos_x, pos_y, center_x, center_y));
        paint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(center_x + pos_x, center_y + pos_y,  setRadiusSize(pos_x, pos_y, canvas), paint);
    }
}
